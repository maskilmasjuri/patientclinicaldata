package org.dxc;

import java.io.*;
import java.util.*;

import org.dxc.entity.ClinicalData;
import org.dxc.entity.Patient;
import org.dxc.factorydesign.HibernateFactory;
import org.dxc.factorydesign.ServiceFactory;
import org.dxc.service.PatientService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class App {

	public static void main(String[] args) throws IOException {
		SessionFactory factory = HibernateFactory.getFactoryObject();

		PatientService service = ServiceFactory.getServiceObject();

		Session session = factory.openSession();

		Transaction tx = session.beginTransaction();

		ClinicalData c1 = new ClinicalData();
		c1.setComponentName("height");
		c1.setComponentValue(176);
		c1.setMeasureDateTime(new Date());

		ClinicalData c2 = new ClinicalData();
		c2.setComponentName("weight");
		c2.setComponentValue(70);
		c2.setMeasureDateTime(new Date());

		ArrayList<ClinicalData> list1 = new ArrayList<ClinicalData>();
		list1.add(c1);
		list1.add(c2);

		Patient p1 = new Patient();
		p1.setFirstName("maskil");
		p1.setLastName("masjuri");
		p1.setAge(30);
		p1.setClinicalData(list1);

		session.persist(p1);

		ClinicalData c3 = new ClinicalData();
		c3.setComponentName("height");
		c3.setComponentValue(155);
		c3.setMeasureDateTime(new Date());

		ClinicalData c4 = new ClinicalData();
		c4.setComponentName("weight");
		c4.setComponentValue(45);
		c4.setMeasureDateTime(new Date());

		ArrayList<ClinicalData> list2 = new ArrayList<ClinicalData>();
		list2.add(c3);
		list2.add(c4);

		Patient p2 = new Patient();
		p2.setFirstName("fiona");
		p2.setLastName("tan");
		p2.setAge(24);
		p2.setClinicalData(list2);

		session.persist(p1);
		session.persist(p2);

		tx.commit();
		session.close();
		System.out.println("Success");

		service.getAllPatients();
	}
}
