package org.dxc.entity;

import java.util.Date;

import javax.persistence.*;

@Entity
public class ClinicalData {
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private int Id;
	
	private String componentName;
	private int componentValue;
	private Date measureDateTime;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public int getComponentValue() {
		return componentValue;
	}
	public void setComponentValue(int componentValue) {
		this.componentValue = componentValue;
	}
	public Date getMeasureDateTime() {
		return measureDateTime;
	}
	public void setMeasureDateTime(Date measureDateTime) {
		this.measureDateTime = measureDateTime;
	}
	
	
}
