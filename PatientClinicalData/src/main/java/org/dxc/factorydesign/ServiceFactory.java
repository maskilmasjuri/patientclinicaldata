package org.dxc.factorydesign;


import org.dxc.service.PatientService;
import org.dxc.service.PatientServiceImplementation;

public class ServiceFactory {
	private static final PatientService service;
	
	static {
		service = new PatientServiceImplementation();
	}
	
	public static PatientService getServiceObject() {
		return service;
	}
}
