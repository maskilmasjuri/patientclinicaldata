package org.dxc.service;

import org.dxc.entity.Patient;

public interface PatientService {
	Integer addPatient(Patient p);
	
	void getAllPatients();
	Patient getByID(int id);
	
}