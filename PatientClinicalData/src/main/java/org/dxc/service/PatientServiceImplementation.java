package org.dxc.service;

import java.util.Iterator;
import java.util.List;

import org.dxc.entity.ClinicalData;
import org.dxc.entity.Patient;
import org.dxc.factorydesign.HibernateFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class PatientServiceImplementation implements PatientService {
	public Integer addPatient(Patient p) {
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();

		Transaction tx = null;
		Integer patientId = null;

		try {
			tx = session.beginTransaction();
			patientId = (Integer) session.save(p);
			tx.commit();
			System.out.println("Patient record created succesfully...");
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return patientId;
	};
	
	public void getAllPatients() {
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();

		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Criteria c = session.createCriteria(Patient.class);
			List patients = c.list();
			for (Iterator iterator = patients.iterator(); iterator.hasNext();) {
				Patient patient = (Patient) iterator.next();
				System.out.print("First Name: " + patient.getFirstName());
				System.out.print("  Last Name: " + patient.getLastName());
				System.out.println("  Age: " + patient.getAge());
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	};
	public Patient getByID(int id) {
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();

		Patient patient = null;
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			patient = (Patient) session.get(Patient.class, id);
			System.out.print("First Name: " + patient.getFirstName());
			System.out.print("  Last Name: " + patient.getLastName());
			System.out.println("  Age: " + patient.getAge());
			
			Criteria c = session.createCriteria(ClinicalData.class);
			c.add(Restrictions.eq("patientid", id));
			List clinicalData = c.list();
			for (Iterator iterator = clinicalData.iterator(); iterator.hasNext();) {
				ClinicalData component = (ClinicalData) iterator.next();
				System.out.print("Name: " + component.getComponentName());
				System.out.print("  Value: " + component.getComponentValue());
				System.out.println("  Date and Time: " + component.getMeasureDateTime());
			}

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return patient;
	};
	
}